#include <fstream>
#include <darknet.h>

#include "dataset.hpp"
#include "fs.hpp"

void nllk::addToDataset(const std::string& labelFileName, const std::string& imageFileName)
{
    std::ofstream labelFile(labelFileName, std::ios::app);

    labelFile << getAbsolutePath(imageFileName) << "\n";

    labelFile.close();
}

void nllk::trainOnSamples(char *dataCfg, char *netCfg, char *weightsFile, int *gpus,
                          int ngpus, int calc_map, float thresh, float iou_thresh,
                          int benchmark_layers, char *chartPath)
{
    train_detector(dataCfg, netCfg, weightsFile, gpus, ngpus, false, true,
                   calc_map, thresh, iou_thresh, -1, false, benchmark_layers,
                   chartPath);
}
