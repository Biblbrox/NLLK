#ifndef FS_HPP
#define FS_HPP

const std::string resPath = "../res/";
const std::string nnPath = "../nn/";
const std::string datasetPath = "../dataset/";

inline std::string getResourcePath(const std::string& fileName)
{
    return resPath + fileName;
}

inline std::string getNNPath(const std::string& fileName)
{
    return nnPath + fileName;
}

inline std::string getDatasetPath(const std::string& fileName)
{
    return datasetPath + fileName;
}

// TODO: enable c++17 features?
#if __cplusplus >= 201703L
#include <filesystem>
inline std::string getAbsolutePath(const std::string& fileName)
{
    return std::filesystem::canonical(std::filesystem::absolute(fileName));
}
#else
#  if defined (__unix__) || (defined (__APPLE__) && defined (__MACH__))
#    include <limits.h>
#    include <stdlib.h>

inline std::string getAbsolutePath(const std::string& fileName)
{
    char resolvedPath[PATH_MAX];
    realpath(fileName.c_str(), resolvedPath);
    return resolvedPath;
}

#  elif defined(_WIN32)
#    include <windows.h>

inline std::string getAbsolutePath(const std::string& fileName)
{
    char resolvedPath[MAX_PATH];
    GetFullPathName(fileName.c_str(), MAX_PATH, resolvedPath, nullptr);
    return std::string(resolvedPath);
}

#  endif
#endif



#endif //FS_HPP
